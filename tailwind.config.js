const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  content: ['./src/**/*.{html,js}'],
  important: true,
  theme: {
    colors: {
      'white': '#fff',
      'black': {
        DEFAULT: '#000',
        '400': '#161513'
      },
      'grey': {
        DEFAULT: '#d0d5d1',
        '800': '#f1e7e5',
        '700': '#636466',
        '600': '#a1a396',
        '500': 'c9b5ae',
        '400': '#eff3f4',
        '300': '#faf9f6',
        '200': '#f9f8f6'
      },
      'pink': {
        DEFAULT: '#f9f3f3',
      }
    },
    screens: {
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px',
      '2xl': '1536px'
    },
    // fontSize: {

    // },
    lineHeight: {
      10: '1',
      11: '1.1',
      12: '1.2',
      125: '1.25',
      13: '1.3',
      14: '1.4',
      15: '1.5',
      16: '1.6',
      17: '1.7',
      175: '1.75'
    },
    fontFamily: {
      default: ['Roboto'],
      'playfair-display': ['Playfair Display']  
    },
    container: {
      center: true,
      padding: '28px', 
      screens: { 
        'desktop': '1300px',
      },
    },
  },
  variants: {
    extend: {
      borderWidth: ['hover', 'last', 'first'],
      fontWeight: ['hover', 'active'],
    },
  },
  plugins: [require('@tailwindcss/forms', {
    // strategy: 'base',
  })],
};
